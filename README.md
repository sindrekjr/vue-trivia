# Vue Trivia Game
A simple game where you will be given trivia questions pulled from the public API at [opentdb](https://opentdb.com/). The basic structure of the project is put together by the @vue/cli demo project. 

## Setup
```
> npm install
> npm run serve
```
The latter line will compile and run a hot deployment of the application for development purposes. Pages are automatically reloaded on any save. 

## Build
The project can be built with Docker or NPM independently. There is also a heroku configuration defined in heroku.yml, which relies on the Dockerfile. 

To simply build the project with NPM locally: 
```
> npm run build
```

## Documentation 
* [Vue.js](https://vuejs.org/v2/guide/)
* [Vue CLI](https://cli.vuejs.org/guide/)
* [Docker](https://docs.docker.com/)
* [Heroku](https://devcenter.heroku.com/categories/reference)
