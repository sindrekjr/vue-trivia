import Vue from "vue";
import VueRouter from "vue-router";

import App from "./App.vue";
import Login from "./containers/Login.vue";
import Game from "./containers/Game.vue";
import GameOver from "./containers/GameOver.vue";

Vue.use(VueRouter);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router: new VueRouter({
    routes: [
      {
        path: "/",
        component: Login
      },
      {
        name: "game",
        path: "/game",
        component: Game,
        props: true
      },
      {
        name: "gameover",
        path: "/gameover",
        component: GameOver,
        props: true
      }
    ]
  })
}).$mount("#app");
