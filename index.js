'use strict'; 

const express = require("express"); 
const morgan = require("morgan"); 

/**
 * Application
 * @ignore
 */
const app = express(); 

app.use(morgan("tiny")); 
app.use(express.static("dist")); 

let port = process.env.PORT || 3000; 

app.listen(port, console.log(`Listening on port ${port}`)); 
